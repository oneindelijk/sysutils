#!/bin/bash
cfg=~/.config/ssh_tunnel.cfg
[[ ! -e ${cfg} ]] && echo Config ${cfg} does not exist && exit 1
source ${cfg}
logfile="/var/log/nas_connect_${name}.log"
set -x
version="0.4"

function check_running() {
    if [[ $(ps aux | grep ssh | grep -v grep | grep ${port} > /dev/null;echo $?) -ne 0 ]]
    then
        connstat=Inactive
    else
        connstat=Active
    fi
}
function connect_tunnel() {
    ssh -fN -R ${port}:localhost:22 -p ${server_port} ${server} & echo $?
}

function remote_log(){
    ssh -p $port ${server} "printf '%s:%s:%s\n' $(date +%Y-%m-%d_%H:%M) ${name} ${@} >> remote/${name}.log"
}

function create_log() {
	sudo touch "$logfile"
	sudo chmod 777 "$logfile"
}

function log(){
    msg="${@}"	
    [[ ! -f "$logfile" ]] && create_log
    printf "%s: %s\n" "$(date)" "${msg}" >> "$logfile"
}

check_running
if [[ ${connstat} != Active ]]
then
    log "No connection detected. Trying to connect..."
    conn_result=$(connect_tunnel)
    log "Connection Result ${conn_result}"
    check_running
    if [[ ${connstat} != Active ]]
    then
        log "Connection NOT restored"
       
        connect_tunnel
    fi 
fi
set +x
log "Connection ${connstat} ($server:$server_port - $port)"
remote_log ${connstat}
