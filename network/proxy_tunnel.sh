#/bin/bash

# A script that opens a pane on the top, displaying which ports are open
# Shows in the first pane a menu to start some tunnels

configfile=~/.config/tunnels.cfg


function read_config() {
    # Implementation to read variables from the config, seeding with defaults if it doesn't exist
    config_exists=true

    # Check if config file exists
    if [ ! -f "$configfile" ]; then
        config_exists=false
        # Create the config file with default content
        echo "name=PokemonProxy;tunnel=-D0.0.0.0:6000;sever=nas" > "$configfile"
        echo "name=PokemonPostgreSQL:tunnel=-L5432:localhost:5432;server=nas" >> "$configfile"
    fi

    # Read variables from the config file
    while IFS=';' read -r name_tunnel_server; do
        # Split the line into name, tunnel, and server
        IFS='=' read -r name tunnel_server <<< "$name_tunnel_server"
        
        # Split tunnel_server into tunnel and server
        IFS=';' read -r tunnel server <<< "$tunnel_server"

        # Use the variables as needed (e.g., store in an array or perform other actions)
        echo "Name: $name, Tunnel: $tunnel, Server: $server"
   
    done < "$configfile"
}


function launch_pane(){
    echo .
    # if the pane isn't running yet, launch a pane with he command
    # watch 'ss -anlp | grep <ports>'
    # substitute <ports> with the ports from the config
    # make the pane 6 lines tall, put it on the top 
    # name it 'watch_pane' so it is easy to find out if the pane was created
}

function menu(){
    echo .
    # list the tunnels available from the config and let the user pick one to start
    # color the running ones in a different color
    # let the user know that '*' can be used to add a new tunnel to the configuration
}

function list_panes() {
    echo .
    # return a list of the pane's names
}

function check_watch_pane() {
    echo .
    # checks if the name watch_pane is created in byobu
}

function add_new_tunnel(){
    echo .
    # help the user create a new tunnel, wizard style
}

function create_tunnel(){
    echo .
    # if a tunnel doesn't exist
    # create it using the ssh options:
    # -fN, StrictHostKeyChecking=no

}

read_config