#!/bin/bash
scan_file_base=/tmp/nmapscan

function get_networks() {
    count=0
    while read line
    do 
        if [[ ! $networks =~ $line ]]
        then
            count=$((count + 1))
            networks="$line;$networks"
        fi
    done < <(ip -br r | grep link | awk -F "/" '{print $1}')
}
function do_scan() {
    nmap -p 22,8022 $network/24 -oG "${scan_file}"  1&> /dev/null
}

function get_open_port() {
    port=$1
    grep -e ' '$port'/open' $scan_file | awk -F " " '{printf "%s;%s\n",$2,$3}' 
}
function check_ssh_config() {
    mac=$1
    ipaddress=$2
    current="$(grep -i "hostname.*$mac" ~/.ssh/config)"
    if [[ -n $current ]]
    then
        if [[ ! ${current} =~ $ipaddress ]]
        then
            sed -i '/'$mac'/ s:\([0-9.]\+\)\ :'$ipaddress' :g' ~/.ssh/config
            printf "Ipaddress for mac $mac updated: $ipaddress\n"
        fi
    fi
}
function try_mac() {
    ipaddress=$1
    sudo nmap $ipaddress | grep MAC > /tmp/nmap_ind_$ipaddress
    MAC=$(awk -F " " '{print $3}' /tmp/nmap_ind_$ipaddress)
    mac=${MAC//:/-}
}
function get_port() {
        line="$1"
        # extract the part before ";"
        ipaddress=${line%;*}
        # extract the part after ";"
        part2=${line#*;}
        # remove the right bracket
        no_bracket=${part2/)/}
        # remove the left bracket
        content=${no_bracket/(/}
        # check if it contains a valid mac address
        mac=$(echo $content | grep '[0-9A-F]\{2\}-[0-9A-F]\{2\}-')
        [[ -z $mac ]] && try_mac $ipaddress
}
function check_port () {
    port=$1
    while read line
    do
        get_port "$line"
        check_ssh_config $mac $ipaddress
    done < <(get_open_port $port)
}
function check_all() {
    network=$1
    scan_file="${scan_file_base}_${network}"
    for port in 22 8022
    do
        check_port $port
    done
}

function run_over_networks(){
    i=0
    while [[ $count -gt $i ]]
    do
        i=$((i+1))
        network=$(echo $networks | awk -F ";" '{print $'$i'}')
        [[ -z $network ]] || check_all $network
    done

}    
# do_scan
get_networks
run_over_networks

