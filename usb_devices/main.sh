#!/bin/bash
find /sys/bus/usb/devices/usb*/ -name dev |  while read sysdevpath
 do
    
        syspath="${sysdevpath%/dev}"
        devname="$(udevadm info -q name -p $syspath)"
        if [[ "$devname" == "bus/"* ]] 
        then
            udevinfo="$(udevadm info -q property --export -p $syspath)"
        fi
        fields=""
        values=""
        for line in ${udevinfo}
        do
            for field in ID_SERIAL DEVPATH DEVNAME PRODUCT ID_MODEL SHORT DATABASE VENDOR
            do
                if [[ "$line" =~ $field ]] 
                then
                    fields="${fields[@]} ${field}"
                    value="${line#*=}"
                    [[ "$value" =~ "devic" ]] && value=$(echo $value | sed 's:/devic.*\:[0-9.]\+\/::g')
                    [[ "$value" =~ "dev" ]] && value=$(echo $value | sed 's:/devic.*\:[0-9.]\+\/::g')
                    # exit 
                    values="${values[@]} ${value}"

                fi
            done
        done
        printf "%s "  "${values[@]}"  
        printf "\n"
        
        # echo "/dev/$devname $ID_SERIAL $syspath" | column

done # | awk -v var="$fields" 'begin {getline title < var;print title }{print $1 $2 $3 $4 $5 $6 $7 $8}'
