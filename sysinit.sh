#!/bin/bash

# little script to figure out what's going on without keyboard access

bootinfo=/root/bootinfo
function do_init() {
  [[ -e .initcount ]] && counter=$(cat .initcount)
  counter=$((counter + 1))
  echo $counter > .initcount
  [[ ! -d ${bootinfo} ]] && mkdir ${bootinfo} && log Created ${bootinfo}
}

function log() {
	timestamp=$(date '+%F %T')
	msg=${@}
	printf "%s || %s\n" "${timestamp}" "${msg}" >> ${bootinfo}/sysinfo.log
}

function check_info() {
	dumpdir=$1
	ip a > ${dumpdir}/ip_info.txt
	for svc in ssh dhc wpa iptables firewalld cronie
	do
		log "Listing services for ${svc}"
		systemctl list-unit-files | grep ${svc} &>> ${dumpdir}/svc.txt 
		systemctl list-units | grep ${svc} &>> ${dumpdir}/svc.txt
	done
	fdisk -l >> ${dumpdir}/fdisk.txt
	ss -anp | grep 22 &>> ${dumpdir}/netstat.txt
	ip l >> ${dumpdir}/ip_info2.txt
	insmod i915 & >> ${dumpdir}/insmod.txt
	insmod usbhid & >> ${dumpdir}/insmod.txt
	lsmod & >> ${dumpdir}/lsmod.txt
	uname -a & >>  ${dumpdir}/uname.txt
}

do_init
check_info ${bootinfo}
