#!/bin/bash

### A script to filter out unwanted mounts from the system and display the remainder in a neat table 
### BY: oneindelijk@gmail.com
version='2.0.1'

filter="snapd tmpfs ramfs"

escaped_filter=${filter// /\\\|}

mount | grep -e '^/' | grep -v -e "$escaped_filter" | awk '{printf "%-20s %s\n",$1,$3}'
