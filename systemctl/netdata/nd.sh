# The Main interface for netdata

# run without options netdata show a list of hosts
# 
# run with a keyword like
# nd <ipaddress> <hostname> <mac-address> will return 
version='0.1.3'
argvars=$@ 
argnums=${#@}
source netdata.env
source parser.sh

function check_pattern(){
    echo 0
}
function known_hostname(){
    showdata
    
}

function parse_args(){
    [[ ${argnums} -eq 0 ]] && showdata && exit 0
            
    if [[ ${argnums} -eq 1 ]]
    then
        known_hostname
        check_pattern
    fi
}


parse_args