#!/bin/bash
version="0.0.3"
sudo drill axfr blue.tesla-intergalactic.org | grep "IN A" | while read line
do
       	hst=${line/.*}
	ip=${line//*A}
	printf " %-15s%sn" $hst $ip
done
