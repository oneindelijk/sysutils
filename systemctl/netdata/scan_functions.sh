#!/bin/bash
version="0.2.4"
# subnet=$(./get_subnets.sh)
# subnet_name=subnet_${subnet/\//_}
# subnet_scanfile=${subnet_name}/nmapscan.csv
# subnetdata=${subnet_name}/data.csv
# portlistfile=ports.list
# portlist=$(tr "\n" "," < $portlistfile)
# global_scan_timeout=600 # minutes 
# single_host_timeout=7200

function update_data_file(){
	field=$1
	shift
	data=$@
	host_record="$(grep $host $subnetdata)"
	if [[ $host_record =~ $field ]]
	then
		#printf "Record found: %s :: %s\n" "$field" "$data"
		sed -i '/'$host'/ s:'$field'=.*;:$field=$data;:' $subnetdata
	else
		printf "%s;%s=%s;\n" "$host_record" "$field" "$data" >> $subnetdata

	fi
}
function get_arp(){
	arpage=$(stat -c '%y' .arp)
	now=$(date +%s)
	if [[ $((now - arpage)) -gt 300 ]]
	then
	   arp -a > .arp
	fi
	mac=$(sed -ne '/'$host'/ s:.*at\ \(.*\)\ \[et.*:\1:p' .arp)
	if [[ -n $mac ]]
	then
		update_data_file MAC $mac
	fi
}

function get_host_list(){
	# query the data file for all mac addresses
	get_ips_from_file
}


function get_ips_from_file(){
	ips=( $(sed -ne '/Host:/ s:.*\ \([0-9.]\+\)\ .*:\1:p' $subnet_scanfile) )
}

function clean_data_file(){
	get_host_list
}
function timeout_check(){
	file_to_check="$1"
	[[ ! -f ${file_to_check} ]] && return 0
	scope=$2
	now=$(date +%s)
	mfile=$(stat -c '%Y' "$file_to_check")
	file_age=$(((now - mfile) / 60 ))
	if [[ $file_age -gt ${!scope} ]]
	then
		return 0
	else
		return 1
	fi
}

# def populate_data(){

# 	ls ${subnet_name} | grep tmp | while read record 
# 	do
# 		printf "%s\n"
# 	done
	
# }

function refresh_subdata(){
	get_ips_from_file
	loop_hosts
	loop_hosts_slow
	# populate_data
}

function scan_network(){
	printf "Scanning %s into %s\n" "$subnet" "$subnet_scanfile"
	if timeout_check ${subnet_scanfile} global_scan_timeout
	then
		sudo nmap -sP -oG "${subnet_scanfile}" "${subnet}" &> /dev/null
	fi
	refresh_subdata
}


function get_pid() {
    argcheck 1 ${@}
	pfile="$1"
	pidfile="/tmp/${pfile}.pid"
	if [[ -f "${pidfile}" ]] 
	then
	  return $(cat "$pidfile")
	else
	  return 1
	fi 
}

function check_pid() {
    argcheck 1 ${@}
	pfile="$1"
	pidfile=/tmp/${pfile}.pid
	if [[ -f ${pidfile} ]] 
	then
	  oldpid=$(get_pid $pidfile)
	  [[ -z $oldpid ]] && echo NO PID FILE FOUND && return 3
	  if [[ $(ps aux | awk '{print $2}' | grep $oldpid)  ]]  #  &> /dev/null)  ]]
	  then
		echo process [$oldpid] still running
		return 0
	  else
	  	echo PIDFILE WAS NOT REMOVED by $oldpid
		rm -f $pidfile
		return 2
	  fi
	else
	  return 1
	fi 
}
function set_pid() {
    argcheck 2 ${@}
	pfile="$1"
	PID="$2"
	pidfile=/tmp/${pfile}.pid
	echo $PID > $pidfile 
}
function remove_pid() {
    argcheck 1 ${@}
	pfile="$1"
	pidfile=/tmp/${pfile}.pid
	pid=$(get_pid "${pfile}")
	if [[ -z $pid ]]
	then
	  rm $(pidfile)
	  echo CANT REMOVE $pfile: RUNNING PID: ${get_pid}
	else
	  if [[ -z $PID ]]
	  then 
	    echo no PID for $pidfile
	  else
	   echo $PID > $pidfile
	 fi
	fi 
}

function bg_runner(){
	bg_target="$1"
	destfile="${2}"
	sleep 2 
	pid=$(get_pid "${bg_target}")
    # sudo nmap -T4 ${target} -oG $destfile &> /dev/null
    printf "[%s] Runner $PID Started\n," "$(date +%H:%M:%S)" 
	nano_part=$(date +%N)
	sleeprandom=${nano_part:0:2}
	sleep ${sleeprandom}
	
	pid=$(get_pid "${bg_target}")
	
    printf "[%s] Runner $PID Stopped\n" "$(date +%H:%M:%S)" 

}
function slow_scan() {
    # function prepare
	target=${1}
	targetfile="${subnet_name}/slow_scan_${target}.tmp"
	sbgfile="${subnet_name}/.${target}.sbg"
	printf "[%s] Starting a slow scan for %s ...\n" "$(date +%H:%M:%S)"  "$target"
	# calls background function if number of running is under threshhold
	bg_runner "$target" &> "${sbgfile}" &
	PID=$!
}

function quickscan_host(){
	host=$1
	printf "Scanning ports for %s\n" $host
	
	hosttmpfile=${subnet_name}/${host}.tmp
	fbgfile="${subnet_name}/.${host}.fbg"
	if timeout_check ${hosttmpfile} single_host_timeout
	then
	       
		bgquickscan "${hosttmpfile}" &>  "${fbgfile}" &
		PID=$!
		set_pid ${host} ${PID}
		sleep .1
		
	else
		printf "No rescanning. Within Timeout period %s\n" "$single_host_timeout"
	fi
	
}

function bgquickscan() {
	
	targetfile="${1}"
	
	printf "Preparing to start Quick BG Scan...\n"
	sleep .3
	bgpid=$(get_pid "$host")
	printf "[%s] Starting a scan to %s (%s) PID: [%s]\n" "$(date +%H:%M:%S)" "$targetfile" "$host" "$bgpid"
	while $(check_pid "$host")
	do	
		[[ -z $show ]] && show=1 && printf "already a scan for %s with PID (%s)\n" "$host" "$bgpid"
		sleep 10
	done
	printf "[%s] Starting a scan for %s [%s]\n" "$(date +%H:%M:%S)" "$host" "$bgpid"
	sudo nmap -p $portlist $host -oN $targetfile &> /dev/null 
	remove_pid ${host}
	nano_part=$(date +%N)
	sleeprandom=${nano_part:0:2}
	sleep ${sleeprandom}
	printf "[%s] Scan Finished for %s\n" "$(date +%H:%M:%S)" "$host"

}
function loop_hosts(){
  for host in ${ips[@]}
  do
    
	quickscan_host $host 
	
	# get_arp $host
  done
}
sleep 20
function loop_hosts_slow(){
  for host in ${ips[@]}
  do
    
	slow_scan $host 
	# allow time to check for amount of running scans
	sleep .1
	# get_arp $host
  done
}

function argcheck(){
	# throw a message if not the expected arguments are received
	argsnum=${1}
	shift
    [[ ${#@} -ne ${argsnum} ]] && echo not the right amount of arguments got ${#@}, expected ${argsnum} && return 1
}