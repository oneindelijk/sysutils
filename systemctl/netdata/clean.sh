#!/bin/bash
set +e
version="0.1.4"
arg=$1
cleanlevel=${arg:=9}   ## 0 == ALL, 100 = None
tdir=~/netdata

if [[ $(pwd) != $tdir ]]
then
  echo Not in the right Folder $tdir
  exit 1 
fi

cpath=subnet_192.168.239.0_24
# rm -fv $cpath/192.168.239.121.tmp
function clean_tmp_subset(){
    exclude=$(ls $cpath | grep tmp | head -n10 | tail -n4 | xargs -i printf " -v %s" {} )
    ls $cpath | while read cargs 
    do 
        [[ ! ${exclude} =~ "$cargs" ]] && rm -v $cpath/$cargs
    done
    ls -l | grep tmp $cpath | grep ${exclude} | while read tmpf
    do
      rm -fv $tmpf
    done
}
function kill_all_mon_processes(){
    ps aux | grep monito | grep -v grep | awk '{print $2}' | xargs -i kill {}

}
# always do these
if [[ $cleanlevel -lt 40 ]]
then
   kill_all_mon_processes
  
fi
if [[ $cleanlevel -lt 1 ]]  # complete reset of the app
then
   rm -vf $cpath/* 
   rm -f transaction*
   rm -f 
   exit 0
fi
if [[ $cleanlevel -lt 10 ]] # reset of the database, scandata stays in place
then
    rm -vf $cpath/data.csv
    mv transaction.log logs/transaction.log$(ls transaction.log* | wc -l)
    
fi

if [[ $cleanlevel -lt 30 ]] # clean the quickscan files
then
   rm -vf $cpath/*.tmp
   
fi

if [[ $cleanlevel -lt 40 ]] # clean the global scan file
then
   rm -vf $cpath/nmapscan.csv
   exit 0
fi

if [[ $cleanlevel -lt 50 ]] # clean a subset of the quickscan files
then
     
    clean_tmp_subset
fi
