#!/bin/bash

version="0.1.30"
source netdata.env
function init(){
    touch ~/.nm_init_file
    [[ ! -f ${subnetdata} ]] && mkdir -p ${subnet_name} && touch ${subnetdata} 
}

function parse_host_files(){
    [[ ! -f ~/.nm_init_file ]] && init
    find ${subnet_name} -name '*.tmp' | while read hosttmpfile
    do
      extract_data "${hosttmpfile}"

      unset CURRENT_RECORD
      unset RECORD
    done
}

function get_own_mac(){
    mac=$(ip a | grep -B1 $IP | sed -ne '/link/ s:.*ether\ \(.*\)\ brd.*:\1:p')
    echo ${mac^^}
}

function get_time(){
    
    timestamp=$(sed -ne '/Nmap done/ s:.*at\ \(.*\)\ --.*:\1:p' $file)
    monotonic=$(date -d "$timestamp" +%s)
    printf "$monotonic"
}

function get_time_records(){
   record=$(sed -ne '/Nmap done/ s:.*at\ \(.*\)\ --.*\([01]\)\ host[s]*\ .*in\ \(.*\):|time=\1||up=\2||scan_time=\3||:p' $file) 
   printf "$record"
}
 
function encode(){
    brol="$@"
    echo "$brol" | base64
}

function extract_data(){
    file="$1"
    if grep MAC $file > /dev/null
    then
        MAC=$(grep -o -e '\([0-9A-F]\{2\}:\)\{5\}[0-9A-F]\+' $file)
    else
        # MAC is missing, this is us
        bn=$(basename $file)
        IP=${bn%.tmp}
        MAC=$(get_own_mac)
    fi 
    [[ -z $MAC ]] && echo No MAC found for $host && return 1
    BRAND_E=$(sed -ne '/MAC Address/ s:.*(\(.*\)):\1:p' $file)
    BRAND="${BRAND_E:--empty-}"
    IP=$(sed -ne '/Nmap scan report/ s:.*for\ \(.*\):\1:p' $file)
    if grep open $file > /dev/null
    then
        port_info=( $(grep open $file | awk '{print $1"||"$2"||"$3}')  )
        #printf "%s" "${port_info[@]}"
        #printf "\n"
    fi
    SEEN=$(get_time)
    NMAP_STATUS="$(get_time_records)"
    CURRENT_RECORD=$MAC
    if ! b_record_exists "$MAC";then add_new_record; fi
    get_current_record
    NEWRECORD="$CURRENT_RECORD|" 
    for i in BRAND IP MAC SEEN NMAP_STATUS port_info
    do
        NEWRECORD="${NEWRECORD}"$(update_field "${i}" "${!i}") 
    done 
    if [[ $(encode $NEWRECORD) != $(encode $RECORD) ]]
    then
        update_row "${NEWRECORD}"
    fi

}

function showdata(){
    printf '| VERSION  %s  || FILE: %s || %s\n' "$version" "${subnetdata}" "$(date +'%d/%m %H:%M:%S')"

    awk -F'[|=]+' 'BEGIN { 
                            split($1,rec,"=");
                            
                            printf "%-20s %-15s %-15s %-17s %-15s\n","MAC", "Brand", "IP Address", "Last Seen", "NMAP Status"; 
                            printf "======================================================================================\n";
                         }{ 
                            printf "%-20s %-15s %-15s %-17s %-15s\n", $1, $3, $5, strftime("%H:%M", $8), $16; 
                         } ' ${subnetdata}


}
# function showdata2(){
#     printf '| VERSION  %s  || FILE: %s || %s\n' "$version" "${subnetdata}" "$(date +'%d/%m %H:%M:%S')"

#     # awk -F'[||]+' 'BEGIN { 
#     #     printf "%-20s %-15s %-15s %-17s %-15s\n", "MAC Address", "Brand", "IP Address", "Last Seen", "NMAP Status"; printf "======================================================================================\n"; 
#     #     } 
#     #     { 
#     #         split($1,mac,"||"); 
#     #         printf "%-18s %-10s %-18s %-17s %-14s\n", mac[1]":"mac[2]":"mac[3]":"mac[4]":"mac[5]":"mac[6], $5, $7, strftime("%Y-%m-%d %H:%M:%S", $9), $13;
#     #     }' ${subnetdata}
    
    


# }

function update_row() {
    data="${@}"
    id=${NEWRECORD%%|*}
    sed -i '/^'$id'|/d' ${subnetdata} 
    printf "%s;Update Row %s;%s" "$(date +%s)" "$id" "${subnetdata}" >> transaction.log
    printf "%s\n" "${data}" >> ${subnetdata}
}
function add_new_record(){
    printf "Adding new RECORD: %s|\n" "$CURRENT_RECORD"
    printf "%s|\n" "$CURRENT_RECORD" >> "$subnetdata"
}



function update_field(){
    argsnum=2
    [[ ${#@} -ne ${argsnum} ]] && echo not the right amount of arguments got ${#@}, expected ${argsnum} && return 1
    fieldname="$1"
    fieldvalue="$2"
    field="|$fieldname=$fieldvalue|"
   
    printf "%s" "$field"
    
}

function get_current_record() {
    RECORD=$(grep "^$CURRENT_RECORD|" "$subnetdata")
}
function b_record_exists(){
    argsnum=1
    [[ ${#@} -ne ${argsnum} ]] && echo not the right amount of arguments got ${#@}, expected ${argsnum} && return 1
    key=$1
    if grep "^$key|" "$subnetdata"  > /dev/null
    then 
      return 0
    else
    #   printf "RECORD does not exist"
      return 1
    fi
}


function b_field_exists(){
    argsnum=1
    [[ ${#@} -ne ${argsnum} ]] && echo not the right amount of arguments got ${#@}, expected ${argsnum} && return 1
    fieldname=$1
    
    if [[ $RECORD =~ "|$fieldname="  ]]
    then 
      return 0
    else
      return 1
    fi
}

function lookup(){
    argcheck 2 ${@}
    ID="${1}"
    fieldname=$2

    result=$(sed -ne '/^'${ID}'|/ s&.*|'${fieldname}'=\(.*\)|.*&\1&p' "$subnetdata")
    #v_matches=$(sed "$searchvalue" "$subnetdata")
    echo "$result" 
}

function filter_by_value(){
    argsnum=2
    [[ ${#@} -ne ${argsnum} ]] && echo not the right amount of arguments got ${#@}, expected ${argsnum} && return 1
    fieldname=$1
    searchvalue=$2

    result=$(sed -ne '/|'${fieldname}'=/ s&\(^.*\)|.*|'${fieldname}'=\(.*\)|.*&\1 |2&p' "$subnetdata")
}