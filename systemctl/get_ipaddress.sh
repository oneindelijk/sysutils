#!/bin/bash
printf ' %-20s %-20s %5s\n' NIC IPADDRESS MASK 
ip l | grep UP\ mode | awk -F ':' '{print $2}' | while read nic
do 
    cidr=$(ip a show dev $nic | grep inet\  | awk '{print $2}');
    mask=${cidr#*\/}
    ipaddress=${cidr%\/*}
    printf ' %-20s %-20s %5d\n' $nic $ipaddress $mask 
done