#!/bin/bash

# if executed with no arguments 
# create a new bookmark for current folder, propose a lower case version of the last part of the folder, let the user edit the name
# if a bookmark exists for current folder display current bookmarks as a list
# store the bookmark and folder in a data file

# if executed with an argument "bookmark"
# lookup the path with the bookmark from the datafile
# change the directory to the path

# Set the path to the data file
DATA_FILE="$HOME/.bookmarks"


function alert() {
    printf "%s: %s\n" "$date +%H:%M" "$@"
}
function store_update() {
    sed -i '/^$BOOKMARK_NAME|/d' "$DATA_FILE"
    echo "$BOOKMARK_NAME|$DIR_PATH" >> "$DATA_FILE"
}

function get_bookmark() {
    BOOKMARK=$(grep "^$BOOKMARK_NAME|" "$DATA_FILE")
    if [[ -n ${BOOKMARK} ]]
    then
        DIR_PATH=$(grep "^$BOOKMARK_NAME|" "$DATA_FILE" |tail -n1| cut -d '|' -f 2)
        BOOKMARK_NAME=$(grep "^$BOOKMARK_NAME|" "$DATA_FILE" | tail -n1 |cut -d '|' -f 1)
        VALID_BOOKMARK=true
    else
        alert No such Bookmark: $BOOKMARK
        VALID_BOOKMARK=false
    fi
}

function print_row() {
    printf "  %-14s  %s\n" "${field1}" "${field2}"

}
function bookmarks_table(){
    field1="BOOKMARKNAME"
    field2="PATH"
    print_row
    field1="------------"
    field2="----"
    print_row
    while read record 
    do
        field1="${record%|*}"
        field2="${record#*|}"
        print_row
      

    done < "$DATA_FILE"

}

function display_bookmarks() {
    cut -d '|' -f 1 "$DATA_FILE" | column
}
function get_bookmark_from_path(){
    FIND_PATHS=${1}
    FOUNDBOOKMARK_NAME=$(grep "|$FIND_PATHS$" "$DATA_FILE" | cut -d '|' -f 1)
}
# If executed with no arguments
if [ $# -eq 0 ]; then
  # Get the current directory path and name
  DIR_PATH=$(pwd)
  DIR_NAME=$(basename "$DIR_PATH")
  # Prompt the user to enter a name for the bookmark
  get_bookmark_from_path "$DIR_PATH"
  if [[ $FOUNDBOOKMARK_NAME == $DIR_NAME ]]
  then
  # a bookmark already exists here
    display_bookmarks
  else
    printf "Enter a name for the bookmark [$DIR_NAME]: "
    read $BOOKMARK_NAME
    # Use the lower case version of the directory name if no bookmark name is provided
    BOOKMARK_NAME=${BOOKMARK_NAME:-${DIR_NAME,,}}
    # Store the bookmark and directory path in the data file
    # echo "$BOOKMARK_NAME|$DIR_PATH" >> "$DATA_FILE"
    store_update
  fi
  # Display the current bookmarks
# If executed with an argument "bookmark"
elif [[ $1 == "-w" ]] 
then
    bookmarks_table

else
  BOOKMARK_NAME="$1"
  get_bookmark
  if $VALID_BOOKMARK
  then
    cd "$DIR_PATH"
  else
    display_bookmarks
  fi
  cd "$DIR_PATH"
fi
